<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI;

final class MidJourneyAI
{
    public static function client(string $apiKey): Client
    {
        return self::factory()
            ->withApiKey($apiKey)
            ->make();
    }

    public static function factory(): Factory
    {
        return new Factory();
    }
}
