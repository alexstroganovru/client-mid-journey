<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Requests\Messages;

use AlexStroganovRu\MidJourneyAI\Contracts\SettingContract;
use AlexStroganovRu\MidJourneyAI\Responses\Concerns\ArrayAccessible;

final class ImagesSettings implements SettingContract
{
    use ArrayAccessible;

    private function __construct(
        public readonly string $application_id,
        public readonly string $data_id,
        public readonly string $data_version,
        public readonly string $session_id,
        public readonly string $user_id,
        public readonly string $guild_id,
    ) {
    }

    public static function from(array $attributes): self
    {
        return new self(
            application_id: $attributes['application_id'],
            data_id: $attributes['data_id'],
            data_version: $attributes['data_version'],
            session_id: $attributes['session_id'],
            user_id: $attributes['user_id'],
            guild_id: $attributes['guild_id'],
        );
    }

    public function toArray(): array
    {
        return [
            'application_id' => $this->application_id,
            'data_id' => $this->data_id,
            'data_version' => $this->data_version,
            'session_id' => $this->session_id,
            'user_id' => $this->user_id,
            'guild_id' => $this->guild_id,
        ];
    }
}