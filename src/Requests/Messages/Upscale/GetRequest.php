<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Requests\Messages\Upscale;

use AlexStroganovRu\MidJourneyAI\Contracts\RequestContract;
use AlexStroganovRu\MidJourneyAI\Exceptions\Messages\Upscale\InvalidUpscaleIndexException;
use AlexStroganovRu\MidJourneyAI\Responses\Concerns\ArrayAccessible;

final class GetRequest implements RequestContract
{
    use ArrayAccessible;

    private function __construct(
        public readonly string $channel_id,
        public readonly string $prompt,
        public readonly int $upscale_index = 0,
    ) {
        ($upscale_index < 0 || $upscale_index > 3) && throw new InvalidUpscaleIndexException();
    }

    public static function from(array $attributes): self
    {
        return new self(
            channel_id: $attributes['channel_id'],
            prompt: $attributes['prompt'],
            upscale_index: $attributes['upscale_index'],
        );
    }

    public function toArray(): array
    {
        return [
            'channel_id' => $this->channel_id,
            'prompt' => $this->prompt,
            'upscale_index' => $this->upscale_index,
        ];
    }
}
