<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Requests\Messages\Upscale;

use AlexStroganovRu\MidJourneyAI\Contracts\RequestContract;
use AlexStroganovRu\MidJourneyAI\Responses\Concerns\ArrayAccessible;

final class CreateRequest implements RequestContract
{
    use ArrayAccessible;

    private function __construct(
        public readonly string $channel_id,
        public readonly string $message_id,
        public readonly null|string $custom_id = null,
    ) {
    }

    public static function from(array $attributes): self
    {
        return new self(
            channel_id: $attributes['channel_id'],
            message_id: $attributes['message_id'],
            custom_id: $attributes['custom_id'] ?? null,
        );
    }

    public function toArray(): array
    {
        return [
            'channel_id' => $this->channel_id,
            'message_id' => $this->message_id,
            'custom_id' => $this->custom_id,
        ];
    }
}
