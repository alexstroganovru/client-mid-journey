<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Requests\Messages\Imagine;

use AlexStroganovRu\MidJourneyAI\Contracts\RequestContract;
use AlexStroganovRu\MidJourneyAI\Responses\Concerns\ArrayAccessible;

final class CreateRequest implements RequestContract
{
    use ArrayAccessible;

    private function __construct(
        public readonly string $channel_id,
        public readonly string $prompt,
        public readonly array $attachments = [],
    ) {
    }

    public static function from(array $attributes): self
    {
        return new self(
            channel_id: $attributes['channel_id'],
            prompt: $attributes['prompt'],
            attachments: $attributes['attachments'] ?? [],
        );
    }

    public function toArray(): array
    {
        return [
            'channel_id' => $this->channel_id,
            'prompt' => $this->prompt,
            'attachments' => $this->attachments,
        ];
    }
}
