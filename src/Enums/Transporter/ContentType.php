<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Enums\Transporter;

enum ContentType: string
{
    case JSON = 'application/json';

    case MULTIPART = 'multipart/form-data';
}
