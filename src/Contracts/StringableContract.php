<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Contracts;

interface StringableContract
{
    public function toString(): string;
}
