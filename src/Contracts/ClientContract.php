<?php

namespace AlexStroganovRu\MidJourneyAI\Contracts;

use AlexStroganovRu\MidJourneyAI\Contracts\Resources\ChannelsContract;
use AlexStroganovRu\MidJourneyAI\Contracts\Resources\MessagesContract;
use AlexStroganovRu\MidJourneyAI\Contracts\Resources\UsersContract;

interface ClientContract
{
    public function users(): UsersContract;

    public function channels(): ChannelsContract;

    public function messages(): MessagesContract;
}
