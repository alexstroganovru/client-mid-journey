<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Contracts;

use AlexStroganovRu\MidJourneyAI\ValueObjects\Transporter\Payload;

interface TransporterContract
{
    public function requestObject(Payload $payload): array|string;
}
