<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Contracts;

use ArrayAccess;

interface RequestContract extends ArrayAccess
{
    public function toArray(): array;

    public function offsetExists(mixed $offset): bool;

    public function offsetGet(mixed $offset): mixed;

    public function offsetSet(mixed $offset, mixed $value): never;

    public function offsetUnset(mixed $offset): never;
}
