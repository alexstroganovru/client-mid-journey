<?php

namespace AlexStroganovRu\MidJourneyAI\Contracts\Resources;

use AlexStroganovRu\MidJourneyAI\Responses\Channels\RetrieveResponse;

interface ChannelsContract
{
    public function retrieve(string $channel_id): RetrieveResponse;
}
