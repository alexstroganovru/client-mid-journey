<?php

namespace AlexStroganovRu\MidJourneyAI\Contracts\Resources;

use AlexStroganovRu\MidJourneyAI\Responses\Users\RetrieveResponse;

interface UsersContract
{
    public function me(): RetrieveResponse;
}
