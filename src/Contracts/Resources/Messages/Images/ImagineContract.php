<?php

namespace AlexStroganovRu\MidJourneyAI\Contracts\Resources\Messages\Images;

use AlexStroganovRu\MidJourneyAI\Requests\Messages\Imagine\CreateRequest;
use AlexStroganovRu\MidJourneyAI\Requests\Messages\Imagine\GetRequest;
use AlexStroganovRu\MidJourneyAI\Responses\Channels\Messages\RetrieveResponse;

interface ImagineContract
{
    public function get(GetRequest $request): RetrieveResponse;

    public function create(CreateRequest $request): void;

    public function generate(CreateRequest $request): RetrieveResponse;
}
