<?php

namespace AlexStroganovRu\MidJourneyAI\Contracts\Resources\Messages\Images;

use AlexStroganovRu\MidJourneyAI\Requests\Messages\Upscale\CreateRequest;
use AlexStroganovRu\MidJourneyAI\Requests\Messages\Upscale\GetRequest;
use AlexStroganovRu\MidJourneyAI\Responses\Channels\Messages\RetrieveResponse;

interface UpscaleContract
{
    public function get(GetRequest $request): RetrieveResponse;

    public function getCreateRequest(GetRequest $request): CreateRequest;

    public function create(CreateRequest $request): void;

    public function generate(GetRequest $request): RetrieveResponse;
}
