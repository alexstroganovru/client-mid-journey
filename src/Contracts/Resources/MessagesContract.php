<?php

namespace AlexStroganovRu\MidJourneyAI\Contracts\Resources;

use AlexStroganovRu\MidJourneyAI\Contracts\Resources\Messages\Images\ImagineContract;
use AlexStroganovRu\MidJourneyAI\Contracts\Resources\Messages\Images\UpscaleContract;
use AlexStroganovRu\MidJourneyAI\Responses\Channels\Messages\ListResponse;

interface MessagesContract
{
    public function list(string $channel_id): ListResponse;

    public function imagine(): ImagineContract;

    public function upscale(): UpscaleContract;
}
