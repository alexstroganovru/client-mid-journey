<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Resources;

use AlexStroganovRu\MidJourneyAI\Contracts\Resources\UsersContract;
use AlexStroganovRu\MidJourneyAI\Responses\Users\RetrieveResponse;
use AlexStroganovRu\MidJourneyAI\ValueObjects\Transporter\Payload;

final class Users implements UsersContract
{
    use Concerns\Transportable;

    public function me(): RetrieveResponse
    {
        $payload = Payload::retrieve('users', '@me');

        $result = $this->transporter->requestObject($payload);

        return RetrieveResponse::from($result);
    }
}
