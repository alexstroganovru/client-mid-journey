<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Resources;

use AlexStroganovRu\MidJourneyAI\Contracts\Resources\Messages\Images\ImagineContract;
use AlexStroganovRu\MidJourneyAI\Contracts\Resources\Messages\Images\UpscaleContract;
use AlexStroganovRu\MidJourneyAI\Contracts\Resources\MessagesContract;
use AlexStroganovRu\MidJourneyAI\Resources\Concerns;
use AlexStroganovRu\MidJourneyAI\Resources\Messages\Images\Imagine;
use AlexStroganovRu\MidJourneyAI\Resources\Messages\Images\Upscale;
use AlexStroganovRu\MidJourneyAI\Responses\Channels\Messages\ListResponse;
use AlexStroganovRu\MidJourneyAI\ValueObjects\Transporter\Payload;

class Messages implements MessagesContract
{
    use Concerns\Transportable;

    public function list(string $channel_id): ListResponse
    {
        $payload = Payload::list(sprintf('channels/%s/messages', $channel_id));

        $result = $this->transporter->requestObject($payload);

        return ListResponse::from([
            'channel_id' => $channel_id,
            'messages' => $result,
        ]);
    }

    public function imagine(): ImagineContract
    {
        return new Imagine($this->transporter);
    }

    public function upscale(): UpscaleContract
    {
        return new Upscale($this->transporter);
    }
}
