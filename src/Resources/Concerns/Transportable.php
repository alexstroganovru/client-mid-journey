<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Resources\Concerns;

use AlexStroganovRu\MidJourneyAI\Contracts\TransporterContract;

trait Transportable
{
    public function __construct(private readonly TransporterContract $transporter)
    {
        // ..
    }
}
