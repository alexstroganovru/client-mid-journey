<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Resources\Concerns;

use AlexStroganovRu\MidJourneyAI\Contracts\ResponseContract;

trait ArraySearchable
{
    public static function firstWhere($array, $key, $value = null)
    {
        if ($array instanceof ResponseContract) {
            $array = $array->toArray();
        }

        foreach ($array as $item) {
            if (
                (is_callable($key) && $key($item)) ||
                (is_string($key) && str_starts_with($item->{$key}, $value))
            ) {
                return $item;
            }
        }

        return null;
    }
}
