<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Resources;

use AlexStroganovRu\MidJourneyAI\Contracts\Resources\ChannelsContract;
use AlexStroganovRu\MidJourneyAI\Responses\Channels\RetrieveResponse;
use AlexStroganovRu\MidJourneyAI\ValueObjects\Transporter\Payload;

final class Channels implements ChannelsContract
{
    use Concerns\Transportable;

    public function retrieve(string $channel_id): RetrieveResponse
    {
        $payload = Payload::retrieve('channels', $channel_id);

        $result = $this->transporter->requestObject($payload);

        return RetrieveResponse::from($result);
    }
}
