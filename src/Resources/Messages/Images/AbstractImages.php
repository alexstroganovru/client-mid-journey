<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Resources\Messages\Images;

use AlexStroganovRu\MidJourneyAI\Exceptions\NotInitSettingsException;
use AlexStroganovRu\MidJourneyAI\Requests\Messages\ImagesSettings;
use AlexStroganovRu\MidJourneyAI\Resources\Concerns\ArraySearchable;

abstract class AbstractImages
{
    use ArraySearchable;

    public null|ImagesSettings $settings = null;

    public function getSettings(): ImagesSettings
    {
        return $this->settings;
    }

    public function setSettings(ImagesSettings $settings)
    {
        $this->settings = $settings;

        return $this;
    }

    public function hasSettings(): void
    {
        is_null($this->settings) and throw new NotInitSettingsException();
    }
}