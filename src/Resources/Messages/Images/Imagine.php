<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Resources\Messages\Images;

use AlexStroganovRu\MidJourneyAI\Contracts\Resources\Messages\Images\ImagineContract;
use AlexStroganovRu\MidJourneyAI\Exceptions\Messages\Imagine\ImagineNotFoundException;
use AlexStroganovRu\MidJourneyAI\Requests\Messages\Imagine\CreateRequest;
use AlexStroganovRu\MidJourneyAI\Requests\Messages\Imagine\GetRequest;
use AlexStroganovRu\MidJourneyAI\Resources\Concerns;
use AlexStroganovRu\MidJourneyAI\Resources\Messages;
use AlexStroganovRu\MidJourneyAI\Responses\Channels\Messages\RetrieveResponse;
use AlexStroganovRu\MidJourneyAI\ValueObjects\Transporter\Payload;

final class Imagine extends AbstractImages implements ImagineContract
{
    use Concerns\Transportable;

    public function get(GetRequest $request): RetrieveResponse
    {
        $this->hasSettings();

        $result = (new Messages($this->transporter))->list($request->channel_id);

        $message = self::firstWhere($result->messages, fn ($item) => (
            str_starts_with($item->content, "**{$request->prompt}** - <@{$this->settings->user_id}>") &&
            ! str_contains($item->content, '%') && str_ends_with($item->content, '(fast)')
        ));

        is_null($message) && throw new ImagineNotFoundException();

        return $message;
    }

    public function create(CreateRequest $request): void
    {
        $this->hasSettings();

        $payload = Payload::create('interactions', [
            'type' => 2,
            'application_id' => $this->settings->application_id,
            'guild_id' => $this->settings->guild_id,
            'channel_id' => $request->channel_id,
            'session_id' => $this->settings->session_id,
            'data' => [
                'version' => $this->settings->data_version,
                'id' => $this->settings->data_id,
                'name' => 'imagine',
                'type' => 1,
                'options' => [
                    [
                        'type' => 3,
                        'name' => 'prompt',
                        'value' => $request->prompt,
                    ],
                ],
                'application_command' => [
                    'id' => $this->settings->data_id,
                    'application_id' => $this->settings->application_id,
                    'version' => $this->settings->data_version,
                    'default_permission' => true,
                    'default_member_permissions' => '',
                    'type' => 1,
                    'nsfw' => false,
                    'name' => 'imagine',
                    'description' => 'Create images with Midjourney',
                    'dm_permission' => true,
                    'options' => [
                        [
                            'type' => 3,
                            'name' => 'prompt',
                            'description' => 'The prompt to imagine',
                            'required' => true,
                        ],
                    ],
                ],
                'attachments' => $request->attachments,
            ],
        ]);

        $this->transporter->requestObject($payload);
    }

    public function generate(CreateRequest $request): RetrieveResponse
    {
        $this->create($request);

        sleep(8);

        $getRequest = GetRequest::from($request->toArray());

        $response = null;
        while (is_null($response)) {
            $response = $this->get($getRequest);
            if (is_null($response)) {
                sleep(8);
            }
        }

        return $response;
    }
}
