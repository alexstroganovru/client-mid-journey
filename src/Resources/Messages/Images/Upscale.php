<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Resources\Messages\Images;

use AlexStroganovRu\MidJourneyAI\Contracts\Resources\Messages\Images\UpscaleContract;
use AlexStroganovRu\MidJourneyAI\Exceptions\Messages\Upscale\NoUpscaleActionsException;
use AlexStroganovRu\MidJourneyAI\Exceptions\Messages\Upscale\UpscaleNotFoundException;
use AlexStroganovRu\MidJourneyAI\Requests\Messages\Imagine\GetRequest as GetImagineRequest;
use AlexStroganovRu\MidJourneyAI\Requests\Messages\Upscale\CreateRequest;
use AlexStroganovRu\MidJourneyAI\Requests\Messages\Upscale\GetRequest;
use AlexStroganovRu\MidJourneyAI\Resources\Concerns;
use AlexStroganovRu\MidJourneyAI\Resources\Messages;
use AlexStroganovRu\MidJourneyAI\Responses\Channels\Messages\RetrieveResponse;
use AlexStroganovRu\MidJourneyAI\ValueObjects\Transporter\Payload;
use Throwable;

final class Upscale extends AbstractImages implements UpscaleContract
{
    use Concerns\Transportable;

    public function get(GetRequest $request): RetrieveResponse
    {
        $this->hasSettings();

        $result = (new Messages($this->transporter))->list($request->channel_id);

        $upscale_index = $request->upscale_index + 1;

        $findLine = "**{$request->prompt}** - Image #{$upscale_index} <@{$this->settings->user_id}>";
        $message = self::firstWhere($result, 'content', $findLine);

        if (is_null($message)) {
            $findLine = "**{$request->prompt}** - Upscaled by <@{$this->settings->user_id}> (fast)";
            $message = self::firstWhere($result, 'content', $findLine);
        }

        is_null($message) && throw new UpscaleNotFoundException();

        return RetrieveResponse::from($message);
    }

    public function getCreateRequest(GetRequest $request): CreateRequest
    {
        $this->hasSettings();

        $result = (new Imagine($this->transporter))->get(GetImagineRequest::from($request->toArray()));

        if (! property_exists($result, 'raw_message')) {
            throw new NoUpscaleActionsException();
        }

        try {
            $custom_id = $result->raw_message->components[0]->components[$request->upscale_index]->custom_id;
        } catch (Throwable) {
            $custom_id = null;
        }

        return CreateRequest::from([
            'channel_id' => $request->channel_id,
            'message_id' => $result->id,
            'custom_id' => $custom_id,
        ]);
    }

    public function create(CreateRequest $request): void
    {
        $this->hasSettings();

        $payload = Payload::create('interactions', [
            'type' => 3,
            'guild_id' => $this->settings->guild_id,
            'channel_id' => $request->channel_id,
            'message_flags' => 0,
            'message_id' => $request->message_id,
            'application_id' => $this->settings->application_id,
            'session_id' => $this->settings->session_id,
            'data' => [
                'component_type' => 2,
                'custom_id' => $request->custom_id,
            ],
        ]);

        $this->transporter->requestObject($payload);
    }

    public function generate(GetRequest $request): RetrieveResponse
    {
        $createRequest = $this->getCreateRequest($request);

        $this->create($createRequest);

        sleep(8);

        $response = null;
        while (is_null($response)) {
            $response = $this->get($request);
            if (is_null($response)) {
                sleep(8);
            }
        }

        return $response;
    }
}
