<?php

namespace AlexStroganovRu\MidJourneyAI;

use AlexStroganovRu\MidJourneyAI\Transporters\HttpTransporter;
use AlexStroganovRu\MidJourneyAI\ValueObjects\ApiKey;
use AlexStroganovRu\MidJourneyAI\ValueObjects\Transporter\BaseUri;
use AlexStroganovRu\MidJourneyAI\ValueObjects\Transporter\Headers;
use AlexStroganovRu\MidJourneyAI\ValueObjects\Transporter\QueryParams;
use Http\Discovery\Psr18ClientDiscovery;
use Psr\Http\Client\ClientInterface;

final class Factory
{
    private ?string $apiKey = null;

    private ?ClientInterface $httpClient = null;

    private ?string $baseUri = null;

    private array $headers = [];

    private array $queryParams = [];

    public function withApiKey(string $apiKey): self
    {
        $this->apiKey = trim($apiKey);

        return $this;
    }

    public function withHttpClient(ClientInterface $client): self
    {
        $this->httpClient = $client;

        return $this;
    }

    public function withBaseUri(string $baseUri): self
    {
        $this->baseUri = $baseUri;

        return $this;
    }

    public function withHttpHeader(string $name, string $value): self
    {
        $this->headers[$name] = $value;

        return $this;
    }

    public function withQueryParam(string $name, string $value): self
    {
        $this->queryParams[$name] = $value;

        return $this;
    }

    public function make(): Client
    {
        $headers = Headers::create();

        if ($this->apiKey !== null) {
            $headers = Headers::withAuthorization(ApiKey::from($this->apiKey));
        }

        foreach ($this->headers as $name => $value) {
            $headers = $headers->withCustomHeader($name, $value);
        }

        $baseUri = BaseUri::from($this->baseUri ?: 'discord.com/api/v9');

        $queryParams = QueryParams::create();
        foreach ($this->queryParams as $name => $value) {
            $queryParams = $queryParams->withParam($name, $value);
        }

        $client = $this->httpClient ??= Psr18ClientDiscovery::find();

        $transporter = new HttpTransporter($client, $baseUri, $headers, $queryParams);

        return new Client($transporter);
    }
}
