<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Exceptions;

use Exception;

final class NotInitSettingsException extends Exception
{
    protected $message = 'Not init settings';
}
