<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Exceptions;

use Exception;

final class InvalidArgumentException extends Exception
{
}
