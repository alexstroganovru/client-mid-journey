<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Exceptions;

use Exception;

final class ErrorException extends Exception
{
    public function __construct(private readonly array $contents)
    {
        parent::__construct($contents['message']);
    }

    public function getErrorMessage(): string
    {
        return $this->getMessage();
    }

    public function getErrorType(): ?string
    {
        return $this->contents['type'];
    }

    public function getErrorCode(): ?string
    {
        return $this->contents['code'];
    }
}
