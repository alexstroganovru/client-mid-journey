<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Exceptions\Messages\Upscale;

use Exception;

final class InvalidUpscaleIndexException extends Exception
{
    protected $message = 'Upscale index must be between 0 and 3';
}
