<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Exceptions\Messages\Upscale;

use Exception;

final class NoUpscaleActionsException extends Exception
{
    protected $message = 'No upscale actions found for this message.';
}
