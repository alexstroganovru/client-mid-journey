<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Exceptions\Messages\Upscale;

use Exception;

final class UpscaleNotFoundException extends Exception
{
    protected $message = 'Upscale image not found';
}
