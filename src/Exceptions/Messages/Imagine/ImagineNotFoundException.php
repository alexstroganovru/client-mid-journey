<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Exceptions\Messages\Imagine;

use Exception;

final class ImagineNotFoundException extends Exception
{
    protected $message = 'Imagine image not found';
}
