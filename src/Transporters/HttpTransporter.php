<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Transporters;

use AlexStroganovRu\MidJourneyAI\Contracts\TransporterContract;
use AlexStroganovRu\MidJourneyAI\Exceptions\ErrorException;
use AlexStroganovRu\MidJourneyAI\Exceptions\TransporterException;
use AlexStroganovRu\MidJourneyAI\Exceptions\UnserializableResponse;
use AlexStroganovRu\MidJourneyAI\ValueObjects\Transporter\BaseUri;
use AlexStroganovRu\MidJourneyAI\ValueObjects\Transporter\Headers;
use AlexStroganovRu\MidJourneyAI\ValueObjects\Transporter\Payload;
use AlexStroganovRu\MidJourneyAI\ValueObjects\Transporter\QueryParams;
use Closure;
use GuzzleHttp\Exception\ClientException;
use JsonException;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;

final class HttpTransporter implements TransporterContract
{
    public function __construct(
        private readonly ClientInterface $client,
        private readonly BaseUri $baseUri,
        private readonly Headers $headers,
        private readonly QueryParams $queryParams,
    ) {
        // ..
    }

    public function requestObject(Payload $payload): array|string
    {
        $request = $payload->toRequest($this->baseUri, $this->headers, $this->queryParams);

        $response = $this->sendRequest(
            fn (): \Psr\Http\Message\ResponseInterface => $this->client->sendRequest($request)
        );

        $contents = $response->getBody()->getContents();

        if ($response->getHeader('Content-Type')[0] === 'text/html; charset=utf-8') {
            return $contents;
        }

        $this->throwIfJsonError($response, $contents);

        try {
            $response = json_decode($contents, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $jsonException) {
            throw new UnserializableResponse($jsonException);
        }

        return $response;
    }

    private function sendRequest(Closure $callable): ResponseInterface
    {
        try {
            return $callable();
        } catch (ClientExceptionInterface $clientException) {
            if ($clientException instanceof ClientException) {
                $this->throwIfJsonError(
                    $clientException->getResponse(),
                    $clientException->getResponse()->getBody()->getContents()
                );
            }

            throw new TransporterException($clientException);
        }
    }

    private function throwIfJsonError(ResponseInterface $response, string|ResponseInterface $contents): void
    {
        if ($response->getStatusCode() < 400) {
            return;
        }

        if ($response->getheader('Content-Type')[0] !== 'application/json; charset=utf-8') {
            return;
        }

        if ($contents instanceof ResponseInterface) {
            $contents = $contents->getBody()->getContents();
        }

        try {
            $response = json_decode($contents, true, 512, JSON_THROW_ON_ERROR);

            if (isset($response['error'])) {
                throw new ErrorException($response['error']);
            }
        } catch (JsonException $jsonException) {
            throw new UnserializableResponse($jsonException);
        }
    }
}
