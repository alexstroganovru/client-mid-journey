<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\ValueObjects;

use AlexStroganovRu\MidJourneyAI\Contracts\StringableContract;

final class ResourceUri implements StringableContract
{
    private function __construct(private readonly string $uri)
    {
        // ..
    }

    public static function create(string $resource): self
    {
        return new self($resource);
    }

    public static function list(string $resource): self
    {
        return new self($resource);
    }

    public static function retrieve(string $resource, string $id): self
    {
        return new self("{$resource}/{$id}");
    }

    public function toString(): string
    {
        return $this->uri;
    }
}
