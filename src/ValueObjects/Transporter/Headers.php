<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\ValueObjects\Transporter;

use AlexStroganovRu\MidJourneyAI\Enums\Transporter\ContentType;
use AlexStroganovRu\MidJourneyAI\ValueObjects\ApiKey;

final class Headers
{
    private function __construct(private readonly array $headers)
    {
        // ..
    }

    public static function create(): self
    {
        return new self([]);
    }

    public static function withAuthorization(ApiKey $apiKey): self
    {
        return new self([
            'Authorization' => $apiKey->toString(),
        ]);
    }

    public function withContentType(ContentType $contentType, string $suffix = ''): self
    {
        return new self([
            ...$this->headers,
            'Content-Type' => $contentType->value.$suffix,
        ]);
    }

    public function withCustomHeader(string $name, string $value): self
    {
        return new self([
            ...$this->headers,
            $name => $value,
        ]);
    }

    public function toArray(): array
    {
        return $this->headers;
    }
}
