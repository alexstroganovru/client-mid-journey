<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI;

use AlexStroganovRu\MidJourneyAI\Contracts\ClientContract;
use AlexStroganovRu\MidJourneyAI\Contracts\TransporterContract;
use AlexStroganovRu\MidJourneyAI\Resources\Channels;
use AlexStroganovRu\MidJourneyAI\Resources\Messages;
use AlexStroganovRu\MidJourneyAI\Resources\Users;

final class Client implements ClientContract
{
    public function __construct(private readonly TransporterContract $transporter)
    {
    }

    public function users(): Users
    {
        return new Users($this->transporter);
    }

    public function channels(): Channels
    {
        return new Channels($this->transporter);
    }

    public function messages(): Messages
    {
        return new Messages($this->transporter);
    }
}
