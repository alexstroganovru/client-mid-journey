<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Responses\Users;

use AlexStroganovRu\MidJourneyAI\Contracts\ResponseContract;
use AlexStroganovRu\MidJourneyAI\Responses\Concerns\ArrayAccessible;

final class RetrieveResponse implements ResponseContract
{
    use ArrayAccessible;

    private function __construct(
        public readonly string $id,
        public readonly string $username,
        public readonly null|string $global_name,
        public readonly null|string $avatar,
        public readonly string $discriminator,
        public readonly int $public_flags,
        public readonly int $flags,
        public readonly null|string $banner,
        public readonly null|int|string $banner_color,
        public readonly null|int|string $accent_color,
        public readonly string $bio,
        public readonly string $locale,
        public readonly bool $nsfw_allowed,
        public readonly bool $mfa_enabled,
        public readonly int $premium_type,
        public readonly array $linked_users,
        public readonly null|string $avatar_decoration,
        public readonly string $email,
        public readonly bool $verified,
        public readonly null|string $phone,
    ) {
    }

    public static function from(array $attributes): self
    {
        return new self(
            $attributes['id'],
            $attributes['username'],
            $attributes['global_name'],
            $attributes['avatar'],
            $attributes['discriminator'],
            $attributes['public_flags'],
            $attributes['flags'],
            $attributes['banner'],
            $attributes['banner_color'],
            $attributes['accent_color'],
            $attributes['bio'],
            $attributes['locale'],
            $attributes['nsfw_allowed'],
            $attributes['mfa_enabled'],
            $attributes['premium_type'],
            $attributes['linked_users'],
            $attributes['avatar_decoration'],
            $attributes['email'],
            $attributes['verified'],
            $attributes['phone'],
        );
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'global_name' => $this->global_name,
            'avatar' => $this->avatar,
            'discriminator' => $this->discriminator,
            'public_flags' => $this->public_flags,
            'flags' => $this->flags,
            'banner' => $this->banner,
            'banner_color' => $this->banner_color,
            'accent_color' => $this->accent_color,
            'bio' => $this->bio,
            'locale' => $this->locale,
            'nsfw_allowed' => $this->nsfw_allowed,
            'mfa_enabled' => $this->mfa_enabled,
            'premium_type' => $this->premium_type,
            'linked_users' => $this->linked_users,
            'avatar_decoration' => $this->avatar_decoration,
            'email' => $this->email,
            'verified' => $this->verified,
            'phone' => $this->phone,
        ];
    }
}
