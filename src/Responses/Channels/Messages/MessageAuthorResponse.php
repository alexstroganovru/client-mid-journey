<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Responses\Channels\Messages;

use AlexStroganovRu\MidJourneyAI\Contracts\ResponseContract;
use AlexStroganovRu\MidJourneyAI\Responses\Concerns\ArrayAccessible;

final class MessageAuthorResponse implements ResponseContract
{
    use ArrayAccessible;

    private function __construct(
        public readonly string $id,
        public readonly string $username,
        public readonly null|string $global_name,
        public readonly null|string $avatar,
        public readonly string $discriminator,
        public readonly int|string $public_flags,
        public readonly bool $bot = false,
    ) {
    }

    public static function from(array $attributes): self
    {
        return new self(
            id: $attributes['id'],
            username: $attributes['username'],
            global_name: $attributes['global_name'],
            avatar: $attributes['avatar'],
            discriminator: $attributes['discriminator'],
            public_flags: $attributes['public_flags'],
            bot: $attributes['bot'] ?? false,
        );
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'username' => $this->username,
            'global_name' => $this->global_name,
            'avatar' => $this->avatar,
            'discriminator' => $this->discriminator,
            'public_flags' => $this->public_flags,
            'bot' => $this->bot,
        ];
    }
}
