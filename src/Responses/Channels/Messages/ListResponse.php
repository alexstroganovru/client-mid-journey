<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Responses\Channels\Messages;

use AlexStroganovRu\MidJourneyAI\Contracts\ResponseContract;
use AlexStroganovRu\MidJourneyAI\Responses\Concerns\ArrayAccessible;
use RuntimeException;

final class ListResponse implements ResponseContract
{
    use ArrayAccessible;

    private function __construct(
        public readonly string $channel_id,
        public readonly array $messages,
    ) {
    }

    public static function from(array $attributes): self
    {
        return new self(
            channel_id: $attributes['channel_id'],
            messages: self::setMessages($attributes['messages'])
        );
    }

    public function toArray(): array
    {
        return [
            'channel_id' => $this->channel_id,
            'messages' => self::getMessages($this->messages),
        ];
    }

    private static function setMessages(array $raw_messages): array
    {
        $messages = [];

        foreach ($raw_messages as $message) {
            if (is_array($message)) {
                $messages[] = RetrieveResponse::from($message);
            } elseif ($message instanceof RetrieveResponse) {
                $messages[] = $message;
            } else {
                throw new RuntimeException('Unknown type');
            }
        }

        return $messages;
    }

    private static function getMessages(array $messages): array
    {
        $raw_messages = [];

        foreach ($messages as $message) {
            if ($message instanceof RetrieveResponse) {
                $raw_messages[] = $message->toArray();
            } else {
                throw new RuntimeException('Unknown type');
            }
        }

        return $raw_messages;
    }
}
