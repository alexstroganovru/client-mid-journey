<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Responses\Channels\Messages;

use AlexStroganovRu\MidJourneyAI\Contracts\ResponseContract;
use AlexStroganovRu\MidJourneyAI\Responses\Concerns\ArrayAccessible;

final class MessageAttachmentResponse implements ResponseContract
{
    use ArrayAccessible;

    private function __construct(
        public readonly string $id,
        public readonly string $filename,
        public readonly int $size,
        public readonly string $url,
        public readonly string $proxy_url,
        public readonly int $width,
        public readonly int $height,
        public readonly string $content_type,
    ) {
    }

    public static function from(array $attributes): self
    {
        return new self(
            id: $attributes['id'],
            filename: $attributes['filename'],
            size: $attributes['size'],
            url: $attributes['url'],
            proxy_url: $attributes['proxy_url'],
            width: $attributes['width'],
            height: $attributes['height'],
            content_type: $attributes['content_type'],
        );
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'filename' => $this->filename,
            'size' => $this->size,
            'url' => $this->url,
            'proxy_url' => $this->proxy_url,
            'width' => $this->width,
            'height' => $this->height,
            'content_type' => $this->content_type,
        ];
    }
}
