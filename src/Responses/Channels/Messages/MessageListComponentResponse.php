<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Responses\Channels\Messages;

use AlexStroganovRu\MidJourneyAI\Contracts\ResponseContract;
use AlexStroganovRu\MidJourneyAI\Responses\Concerns\ArrayAccessible;
use RuntimeException;

final class MessageListComponentResponse implements ResponseContract
{
    use ArrayAccessible;

    private function __construct(
        public readonly int $type,
        public readonly array $components = [],
    ) {
    }

    public static function from(array $attributes): self
    {
        return new self(
            type: $attributes['type'],
            components: self::setComponents($attributes['components']),
        );
    }

    public function toArray(): array
    {
        return [
            'type' => $this->type,
            'components' => self::getComponents($this->components),
        ];
    }

    private static function setComponents(array $raw_components): array
    {
        $components = [];

        foreach ($raw_components as $component) {
            if (is_array($component)) {
                $components[] = MessageComponentResponse::from($component);
            } elseif ($component instanceof MessageComponentResponse) {
                $components[] = $component;
            } else {
                throw new RuntimeException('Unknown type');
            }
        }

        return $components;
    }

    private static function getComponents(array $components): array
    {
        $raw_components = [];

        foreach ($components as $component) {
            if (is_array($component)) {
                $raw_components[] = self::getComponents($component);
            } elseif ($component instanceof MessageComponentResponse) {
                $raw_components[] = $component->toArray();
            } else {
                throw new RuntimeException('Unknown type');
            }
        }

        return $raw_components;
    }
}
