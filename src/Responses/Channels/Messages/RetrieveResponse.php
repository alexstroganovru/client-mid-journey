<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Responses\Channels\Messages;

use AlexStroganovRu\MidJourneyAI\Contracts\ResponseContract;
use AlexStroganovRu\MidJourneyAI\Responses\Concerns\ArrayAccessible;
use RuntimeException;

final class RetrieveResponse implements ResponseContract
{
    use ArrayAccessible;

    private function __construct(
        public readonly string $id,
        public readonly string $timestamp,
        public readonly string $channel_id,
        public readonly int $type,
        public readonly string $content,
        public readonly MessageAuthorResponse $author,
        public readonly array $attachments = [],
        public readonly array $components = [],
        public readonly array $embeds = [],
        public readonly array $mentions = [],
        public readonly array $mention_roles = [],
        public readonly bool $mention_everyone = false,
        public readonly bool $pinned = false,
        public readonly bool $tts = false,
        public readonly int $flags = 0,
        public readonly null|string $edited_timestamp = null,
    ) {
    }

    public static function from(array $attributes): self
    {
        return new self(
            id: $attributes['id'],
            timestamp: $attributes['timestamp'],
            channel_id: $attributes['channel_id'],
            type: $attributes['type'],
            content: $attributes['content'],
            author: MessageAuthorResponse::from($attributes['author']),
            attachments: self::setAttachments($attributes['attachments']),
            components: self::setComponents($attributes['components']),
            embeds: $attributes['embeds'],
            mentions: $attributes['mentions'],
            mention_roles: $attributes['mention_roles'],
            mention_everyone: $attributes['mention_everyone'],
            pinned: $attributes['pinned'],
            tts: $attributes['tts'],
            flags: $attributes['flags'],
            edited_timestamp: $attributes['edited_timestamp'],
        );
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'timestamp' => $this->timestamp,
            'channel_id' => $this->channel_id,
            'type' => $this->type,
            'content' => $this->content,
            'author' => $this->author->toArray(),
            'attachments' => self::getAttachments($this->attachments),
            'components' => self::getComponents($this->components),
            'embeds' => $this->embeds,
            'mentions' => $this->mentions,
            'mention_roles' => $this->mention_roles,
            'mention_everyone' => $this->mention_everyone,
            'pinned' => $this->pinned,
            'tts' => $this->tts,
            'flags' => $this->flags,
            'edited_timestamp' => $this->edited_timestamp,
        ];
    }

    private static function setAttachments(array $raw_attachments): array
    {
        $attachments = [];

        foreach ($raw_attachments as $attachment) {
            if (is_array($attachment)) {
                $attachments[] = MessageAttachmentResponse::from($attachment);
            } elseif ($attachment instanceof MessageAttachmentResponse) {
                $attachments[] = $attachment;
            } else {
                throw new RuntimeException('Unknown type');
            }
        }

        return $attachments;
    }

    private static function getAttachments(array $attachments): array
    {
        $raw_attachments = [];

        foreach ($attachments as $attachment) {
            if ($attachment instanceof MessageAttachmentResponse) {
                $raw_attachments[] = $attachment->toArray();
            } else {
                throw new RuntimeException('Unknown type');
            }
        }

        return $raw_attachments;
    }

    private static function setComponents(array $raw_components): array
    {
        $components = [];

        foreach ($raw_components as $component) {
            if (is_array($component)) {
                $components[] = MessageListComponentResponse::from($component);
            } elseif ($component instanceof MessageListComponentResponse) {
                $components[] = $component;
            } else {
                throw new RuntimeException('Unknown type');
            }
        }

        return $components;
    }

    private static function getComponents(array $components): array
    {
        $raw_components = [];

        foreach ($components as $component) {
            if (is_array($component)) {
                $raw_components[] = self::getComponents($component);
            } elseif ($component instanceof MessageListComponentResponse) {
                $raw_components[] = $component->toArray();
            } else {
                throw new RuntimeException('Unknown type');
            }
        }

        return $raw_components;
    }
}
