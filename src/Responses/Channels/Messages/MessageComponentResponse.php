<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Responses\Channels\Messages;

use AlexStroganovRu\MidJourneyAI\Contracts\ResponseContract;
use AlexStroganovRu\MidJourneyAI\Responses\Concerns\ArrayAccessible;

final class MessageComponentResponse implements ResponseContract
{
    use ArrayAccessible;

    private function __construct(
        public readonly string $custom_id,
        public readonly string $label,
        public readonly int $type,
        public readonly int $style,
        public readonly bool $emoji = false,
    ) {
    }

    public static function from(array $attributes): self
    {
        return new self(
            custom_id: $attributes['custom_id'],
            label: $attributes['emoji']['name'] ?? $attributes['label'],
            type: $attributes['type'],
            style: $attributes['style'],
            emoji: isset($attributes['emoji']),
        );
    }

    public function toArray(): array
    {
        return [
            'custom_id' => $this->custom_id,
            'label' => $this->label,
            'type' => $this->type,
            'style' => $this->style,
            'emoji' => $this->emoji,
        ];
    }
}
