<?php

declare(strict_types=1);

namespace AlexStroganovRu\MidJourneyAI\Responses\Channels;

use AlexStroganovRu\MidJourneyAI\Contracts\ResponseContract;
use AlexStroganovRu\MidJourneyAI\Responses\Concerns\ArrayAccessible;

final class RetrieveResponse implements ResponseContract
{
    use ArrayAccessible;

    private function __construct(
        public readonly string $id,
        public readonly int $type,
        public readonly string $last_message_id,
        public readonly int $flags,
        public readonly string $guild_id,
        public readonly string $name,
        public readonly string $parent_id,
        public readonly int $rate_limit_per_user,
        public readonly null|string $topic,
        public readonly int $position,
        public readonly array $permission_overwrites = [],
        public readonly bool $nsfw = false,
    ) {
    }

    public static function from(array $attributes): self
    {
        return new self(
            $attributes['id'],
            $attributes['type'],
            $attributes['last_message_id'],
            $attributes['flags'],
            $attributes['guild_id'],
            $attributes['name'],
            $attributes['parent_id'],
            $attributes['rate_limit_per_user'],
            $attributes['topic'],
            $attributes['position'],
            $attributes['permission_overwrites'],
            $attributes['nsfw'],
        );
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'last_message_id' => $this->last_message_id,
            'flags' => $this->flags,
            'guild_id' => $this->guild_id,
            'name' => $this->name,
            'parent_id' => $this->parent_id,
            'rate_limit_per_user' => $this->rate_limit_per_user,
            'topic' => $this->topic,
            'position' => $this->position,
            'permission_overwrites' => $this->permission_overwrites,
            'nsfw' => $this->nsfw,
        ];
    }
}
