## Mid Journey API

**MidJourney PHP** is a API client that allows you to interact with the [MidJourney AI API]. If you or your business
relies on this package, it's important to support the developers who have contributed their time and effort to create
and maintain this valuable tool:

- Alex Stroganov: **[gitlab.com/alexstroganovru](https://gitlab.com/alexstroganovru)**

## Table of Contents

- [Get Started](#get-started)
- [Usage](#usage)
- [Testing](#testing)

## Get Started

> **Requires [PHP 8.2+](https://php.net/releases/)**

First, install MidJourneyAI via the [Composer](https://getcomposer.org/) package manager:

```bash
composer require alexstroganovru/client-mid-journey
```

Ensure that the `php-http/discovery` composer plugin is allowed to run or install a client manually if your project does
not already have a PSR-18 client integrated.

```bash
composer require guzzlehttp/guzzle
```

Then, interact with MidJourneyAI's API:

```php
$yourApiKey = getenv('YOUR_API_KEY');
$client = MidJourneyAI::client($yourApiKey);

$result = $client->messages()->imagine()->generate(CreateRequest::from([
    'channel_id' => 'YOUR_CHANNEL_ID',
    'prompt' => 'YOUR_PROMPT',
]));

echo $result;
```

## Usage

no content

## Testing

no content

---

MidJourneyAI PHP is an open-sourced software licensed under the **[MIT license](https://opensource.org/licenses/MIT)**.